import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

function ProductCard({ products, addToCart }) {
    console.log(products);

    return (
        <div className="col-9">
            <div className="d-flex flex-wrap bd-highlight mb-3">
                {products.map((product, index) => (
                    <div className="p-2 bd-highlight" key={index}>
                        < Card style={{ width: "200px" }}>
                            <Link to={`product/${product.slug}`}>
                                <Card.Img variant='top' src={product.photoUrl} />
                                {/* <div style={{ backgroundColor: "rgb(124, 196, 48)", height: "200px" }}></div> */}
                                <Card.Body>
                                    <Card.Title>{product.merchant.name}<h5>{product.name}</h5></Card.Title>
                                    <Card.Text>
                                        {product.size}
                                        <br />
                                        Rp {product.price}
                                    </Card.Text>
                                </Card.Body>
                            </Link>
                            <Card.Footer className="p-0">
                                <Button style={{ width: "100%" }} variant="primary" onClick={addToCart}>
                                    Beli
                                </Button>
                            </Card.Footer>
                            {/* <button  type="button" class="btn btn-primary">Beli</button> */}
                        </Card>
                    </div>
                ))
                }
            </div>
        </div>
    );
}

export default ProductCard;