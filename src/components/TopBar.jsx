function TopBar() {
    return (
        <div className="container-fluid top-bar py-2 d-flex flex-wrap justify-content-between">
            <a href="#">Download Aplikasi Lemonilo</a>
            <div>Butuh Bantuan? (+62) 21 4020 0788</div>
            <a href="#">Diskon sebesar IDR 50000 dengan kupon <span style={{ color: "#DE8044" }}>BELANJASEHAT</span> untuk pelanggan
                baru
            </a>
            <a href="#">Kenapa Belanja di Lemonilo?</a>
        </div>
    );
}

export default TopBar;