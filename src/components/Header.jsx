import Navigation from "./Navigation";
import TopBar from "./TopBar";

function Header() {
    return (
        <header className="sticky-top">
            <TopBar />
            <Navigation />
        </header>
    );
}

export default Header;