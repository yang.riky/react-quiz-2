import { Link } from "react-router-dom";
import { Form } from "react-bootstrap";

function Navigation() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <Link to="/" className="navbar-brand">
                    {/* <a className="navbar-brand" href="#"> */}
                    <img src="/lemonilo.png" alt="lemonilo" width="120px" />
                    {/* </a> */}
                </Link>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="#">Kategori</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Promo</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Life</a>
                        </li>
                    </ul>
                    <Form className="d-flex flex-grow-1">
                        <Form.Control className="form-control me-2 input-search" type="search" placeholder="Cari produk apa hari ini?"
                            aria-label="Search">
                        </Form.Control>
                    </Form>
                    <ul className="navbar-nav mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a className="nav-link" href="#">Cart</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Notif</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Masuk</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Daftar</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Navigation;