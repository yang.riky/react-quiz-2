function ProductDropdown() {
    return (
        <div className="col d-flex justify-content-end">
            <p><b>Urutkan</b></p>
            <div className="ms-2 dropdown">
                <button className="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    Rekomendasi Kami
                </button>
                <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a className="dropdown-item" href="#">Terpopuler</a></li>
                    <li><a className="dropdown-item" href="#">Terbaru</a></li>
                    <li><a className="dropdown-item" href="#">Harga: Rendah ke Tinggi</a></li>
                    <li><a className="dropdown-item" href="#">Harga: Tinggi ke Rendah</a></li>
                    <li><a className="dropdown-item" href="#">Rating: Tinggi ke Rendah</a></li>
                </ul>
            </div>
        </div>
    );
}

export default ProductDropdown;