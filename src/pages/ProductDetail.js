import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ProductDetail = () => {
    const API = "https://api.lemonilo.com/v1/";
    const PRODUCT = 'product/';
    const [product, setProduct] = useState([]);
    const [loading, setLoading] = useState(true);
    const params = useParams();
    const { product_slug } = params;

    useEffect(() => {
        fetchProduct();
    }, [product_slug]);

    const fetchProduct = async () => {
        const response = await fetch(
            API + PRODUCT + product_slug, {
            method: 'GET'
        }
        );

        if (!response.ok) {
            setLoading(false);
            // throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        setProduct(data.data);
        setLoading(false);

        console.log(data);
        console.log(product);
    }

    return (
        <div className="container">
            {!loading ? (
                <div>
                    <div className="d-flex">
                        <div className="flex-shrink-0">
                            <img src={product.productImages[0].photoUrl} alt="..." />
                        </div>
                        <div className="flex-grow-1 ms-3">
                            <div>{product.merchant.name}</div>
                            <div>{product.name}</div>
                            <div>{product.size}</div>
                            <div>{product.price}</div>
                        </div>
                    </div>
                    <hr />
                    <div>Rincian Produk</div>
                    <div>{product.expirationStartDate}</div>
                    <div>{product.netWeight * 1000} gr</div>
                    <div>{product.actualWeight * 1000} gr</div>
                    <hr />
                    <div>Deskripsi Produk</div>
                    <div dangerouslySetInnerHTML={{ __html: product.description }} />
                    <hr />
                </div>
            ) : (<div></div>)

            }
        </div>
    );
}

export default ProductDetail;