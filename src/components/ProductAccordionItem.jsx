function ProductAccordionItem(props) {
    return (
        <div className="accordion-item">
            <h2 className="accordion-header" id="heading+${props.title}">
                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${props.title}"
                    aria-expanded="true" aria-controls="collapse${props.title}">
                    {props.title}
                </button>
            </h2>
            <div id="collapse${props.title}" className="accordion-collapse collapse show" aria-labelledby="heading${props.title}"
                data-bs-parent="#accordionExample">
                {props.body.map((b, index) => (
                    <div className="accordion-body" key={index}>
                        {b}
                    </div>
                ))}
            </div>
        </div >
    );
}

export default ProductAccordionItem;