import { useEffect, useState } from "react";
import ProductListBody from "../components/ProductListBody";
import ProductListHeader from "../components/ProductListHeader";
import ProductPagination from "../components/ProductPagination";
import ReactPaginate from "react-paginate";

const Product = () => {
    const API = "https://api.lemonilo.com/v1/";
    const PRODUCT_POPULAR = 'product/popular';
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPage, setTotalpage] = useState(1);

    useEffect(() => {
        fetchProducts(`?page=${currentPage}`);
    }, [currentPage]);

    const fetchProducts = async (params = "") => {
        const response = await fetch(
            API + PRODUCT_POPULAR + params, {
            method: 'GET'
        }
        );

        if (!response.ok) {
            setLoading(false);
            // throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        setProducts(data.data);
        setCurrentPage(data.currentPage);
        setTotalpage(data.totalPages);
        setLoading(false);

        console.log(data);
    }

    const addToCart = () => {
        alert("Add to Cart");
    }

    const handlePageClick = (value) => {
        setCurrentPage(value.selected + 1);
    }

    return (
        <div className="container mt-3">
            {!loading ? (
                <div>
                    <ProductListHeader />

                    <ProductListBody products={products} addToCart={addToCart} />

                    {/* <ProductPagination currentPage={currentPage} totalPage={totalPage} /> */}
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel="next >"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={totalPage}
                        previousLabel="< previous"
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        previousLinkClassName="page-link"
                        nextClassName="page-item"
                        nextLinkClassName="page-link"
                        breakClassName="page-item"
                        breakLinkClassName="page-link"
                        containerClassName="pagination"
                        activeClassName="active"
                        renderOnZeroPageCount={null}
                    />
                </div>
            ) : (<div></div >)}
        </div>
    );
};

export default Product;