import ProductCard from "../components/ProductCard";
import ProductSidebar from "../components/ProductSidebar";

function ProductListBody({ products, addToCart }) {
    return (
        <div className="row row-cols-2 mt-3">
            <ProductSidebar />

            <ProductCard products={products} addToCart={addToCart} />
        </div>
    );
}

export default ProductListBody;