import ProductAccordionItem from "./ProductAccordionItem";

function ProductAccordion() {
    return (
        <div className="accordion" id="accordionExample">
            <ProductAccordionItem title={"Kategori"} body={["Kategori 1", "Kategori 2", "Kategori 3"]} />
            <ProductAccordionItem title={"Manfaat"} body={["Manfaat 1", "Manfaat 2", "Manfaat 3"]} />
            <div className="accordion-item">
                <h2 className="accordion-header" id="headingThree">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                        data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Peringkat
                    </button>
                </h2>
                {/* <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree"
                    data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <input type="range" className="form-range" min="1" max="5" id="customRange2" />
                        <div className="d-flex justify-content-between">
                            <label for="customRange2" className="form-label">1</label>
                            <label for="customRange2" className="form-label">2</label>
                            <label for="customRange2" className="form-label">3</label>
                            <label for="customRange2" className="form-label">4</label>
                            <label for="customRange2" className="form-label">5</label>
                        </div>
                    </div>
                </div> */}
            </div>
            <div className="accordion-item">
                <h2 className="accordion-header" id="headingFour">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                        data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Harga
                    </button>
                </h2>
                {/* <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour"
                    data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        <input type="range" className="form-range" min="100" max="1000000" id="customRange2" />
                        <div className="d-flex justify-content-between">
                            <label for="customRange2" className="form-label">100</label>
                            <label for="customRange2" className="form-label">1.000.000</label>
                        </div>
                    </div>
                </div> */}
            </div>
        </div>
    );
}

export default ProductAccordion;