import ProductDropdown from "./ProductDropdown";
import { Breadcrumb } from "react-bootstrap";

function ProductListHeader() {
    return (
        <div className="row row-cols-1">
            <div className="col">
                <Breadcrumb>
                    <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Produk Laris</Breadcrumb.Item>
                </Breadcrumb>
            </div>

            <ProductDropdown />
            <div className="col"><b>Paling Laris</b></div>
        </div>
    );
}

export default ProductListHeader;