import { Button } from "react-bootstrap";
import ProductAccordion from "./ProductAccordion";

function ProductSidebar() {
    return (
        <div className="col-3 d-none d-md-block">
            <div className="d-flex justify-content-between">
                <b>Filter</b>
                {/* <button type="button" className="btn btn-link link-reset">Reset</button> */}
                <Button variant="link">Reset</Button>
            </div>
            <ProductAccordion />
        </div>
    );
}

export default ProductSidebar;